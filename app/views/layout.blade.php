<!DOCTYPE html>
<html>
<head>
  {{ HTML::style('css/bootstrap.css') }}
  {{ HTML::style('css/sb-admin.css') }}
  {{ HTML::script('js/jquery-1.10.2.js') }}
</head>
<body>
  @include('partials/header')
  @yield('content')
</body>
  {{ HTML::script('js/bootstrap.js') }}
</html>
